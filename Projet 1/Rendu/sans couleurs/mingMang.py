# mingMang project by Thomas Dazy (294828)
# Create in PYTHON 3.7
# rev 5.0

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# ATTENTION: ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 mingMang.py'
# Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#

# Dans tous le programme: board[ligne][colone]

# Code couleur Joueur 1: 34
# Code couleur Joueur 2: 32
# Code couleur de réinitialisation: 0m

def newBoard(n):
    newBoard = []
    newRow = []

    for x in range(0, n, 1):
        newRow = []
        for y in range(0, n, 1):
            newRow.append("0")
        newBoard.append(newRow)
    for x in range(0, n, 1):
       newBoard[0][x] = "2"
    for x in range(0, n, 1):
       newBoard[x][0] = "1"
    for x in range(0, n, 1):
       newBoard[n-1][x] = "1"
    for x in range(0, n, 1):
       newBoard[x][n-1] = "2"
    return newBoard

def display(board, n):
    print("_"*((n*2)+3))
    for x in range(0, n, 1):
        print("| ", end='')
        for y in range(0, n, 1):
            if board[x][y] == "0":
                print(u"\u2022", end=' ')
            elif board[x][y] == "1":
                print("X ", end='')
            elif board[x][y] == "2":
                print("O ", end='')

        print("|")
    print("￣" * (n + 2))


def possiblePawn(board, n, player, i, j):
    # Si le pion séléctionné appartiens au joueur
    if i < n and j < n:
        if int(board[i][j]) == int(player):
            # On vérifie que le pion à de la place pour bouger
            # On vérifie qu'il y ai une case de libre à droite
            if j+1 != n and board[i][j+1] == "0":
                return True
            # On vérifie qu'il y ai une case de libre à gauche
            elif (j-1) >= 0 and board[i][j-1] == "0":
                return True
            # On vérifie qu'il y ai une case de libre en bas
            elif i+1 != n and board[i+1][j] == "0":
                return True
            # On vérifie qu'il y ai une case de libre en haut
            elif i-1 >= 0 and board[i-1][j] == "0":
                return True
            else:
                print("Le pion n'a pas la place pour bouger")
        else:
            print("Le pion ne vous appartiens pas")
    else:
        print("Séléctionnez un pion sur le plateau")

    return False


def selectPawn(board, n, player):
    toReturn = False
    while toReturn == False:
        i = int(userIntInput("Selectionnez une ligne")) - 1
        j = int(userIntInput("Selectionnez une colone")) - 1
        toReturn = possiblePawn(board, n, player, i, j)
    return i, j

def possibleDestination(board, n, i, j, k, l):
    # i and k horizontal
    # j and l vertical
    if i != k and j != l:
        print("Vous ne pouvez bouger que dans une seule direction")

        return False
    if k >= n and l >= n:
        return False

    # Si les pions à déplacer sont sur la même case, on return False:
    if i == k and j == l:
        print("Vous devez déplacer un pion")
        return False
    # Si les pions sont sur la même ligne: i et k sont égaux
    if i == k:
        # On vérifie que entre les pions il n'y ai rien:
        # Si on déplace le pion vers la droite:
        if j < l:
            for x in range(j+1, l+1, 1):
                if board[i][x] != "0":
                    print("Il y a un pion qui bloque le chemin")
                    return False
        # Si on déplace le pion vers la gauche:
        elif j > l:
            for x in range(l, j, 1):
                if board[i][x] != "0":
                    print("Il y a un pion qui bloque le chemin")
                    return False
    # Si les pions sont sur la même colone: j et l sont égaux
    if j == l:
        # On vérifie que entre les pions il n'y ai rien:
        # Si on déplace le pion vers le bas:
        if i < k:
            for x in range(i+1, k+1, 1):
                if board[x][j] != "0":
                    print("Il y a un pion qui bloque le chemin")

                    return False
        # Si on déplace le pion vers le haut:
        if i > k:
            for x in range(k, i, 1):
                if board[x][j] != "0":
                    print("Il y a un pion qui bloque le chemin")

                    return False
    return True

def selectDestination(board, n, i, j):
    toReturn = False
    while toReturn == False:
        k = int(userIntInput("Selectionnez une ligne de destination")) - 1
        l = int(userIntInput("Selectionnez une colone de destination")) - 1
        toReturn = possibleDestination(board, n, i, j, k, l)
    return k, l


def move(board, n, player, i, j, k, l):
    # On considère que l'on à testé si le pion pouvais se déplacer avec ces valeurs

    # 1. On bouge le pion:
    board[i][j] = "0"
    board[k][l] = str(player)

    # 2. On vérifie vers la droite si il y a des pions que l'on peut capturer
    m = l + 1
    while m < n and board[k][m] != str(player) and board[k][m] != "0" and m >= 0:
        m = m + 1
    if m >= 0 and m < n and board[k][m] == str(player):
        # CAPTURE VERS LA DROITE!!!
        m = l + 1
        while m < n and m >= 0 and board[k][m] != str(player) and board[k][m] != "0":
            board[k][m] = str(player)
            m = m + 1

    # 3. On vérifie vers la gauche si il y a des pions que l'on peut capturer
    m = l - 1
    while m < n and m >= 0 and board[k][m] != str(player) and board[k][m] != "0":
        m = m - 1
    if m >= 0 and m < n and board[k][m] == str(player):
        # CAPTURE VERS LA GAUCHE!!!
        m = l - 1
        while m < n and m >= 0 and board[k][m] != str(player) and board[k][m] != "0":
            board[k][m] = str(player)
            m = m - 1

    # 4. On vérifie vers le bas si il y a des pions que l'on peut capturer
    m = k + 1
    while m < n and m >= 0 and board[m][l] != str(player) and board[m][l] != "0":
        m = m + 1
        if m == 10:
            m = 9
    if m >= 0 and m < n and board[m][l] == str(player):
        # CAPTURE VERS LE BAS!!!
        m = k + 1
        while m < n and m >= 0 and board[m][l] != str(player) and board[m][l] != "0":
            board[m][l] = str(player)
            m = m + 1

    # 5. On vérifie vers le bas si il y a des pions que l'on peut capturer
    m = k - 1
    while m < n and m >= 0 and board[m][l] != str(player) and board[m][l] != "0":
        m = m - 1
    if m >= 0 and m < n and board[m][l] == str(player):
        # CAPTURE VERS LE HAUT!!!
        m = k - 1
        while m < n and m >= 0 and board[m][l] != str(player) and board[m][l] != "0":
            board[m][l] = str(player)
            m = m - 1


def lose(board, n, player):
    # Pour toutes les colones
    for x in range(0, n, 1):
        # Pour toutes les lignes
        for y in range(0, n, 1):
            # Si on a
            if board[x][y] == str(player):
                # On vérifie qu'il y ai une case de libre à droite
                if board[x][y+1] == "0" and y+1 != n:
                    return False
                # On vérifie qu'il y ai une case de libre à gauche
                if board[x][y-1] == "0" and (y-1) >= 0:
                    return False
                # On vérifie qu'il y ai une case de libre en bas
                if board[x+1][y] == "0" and x+1 != n:
                    return False
                # On vérifie qu'il y ai une case de libre en haut
                if board[x-1][y] == "0" and x-1 >= 0:
                    return False
    # Si on a trouvé aucuns pions qui peuvent être déplacés, on renvoie True
    return True

def displayPlayer(board, n, player):

    print("Au tour du joueur ", end='')
    if player == 1:
        print("1")
    elif player == 2:
        print("2")

def changePlayer(board, n, player):
    if player == 1:
        return 2
    elif player == 2:
        return 1

def initialisation():
    print("\n"*100)
    print("ATTENTION, ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 <nom_du_fichier>'")
    print("")
    print("Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci")
    n = int(userIntInput("Combiens de lignes et de colones voulez-vous ?"))

    mingMang(n)

def userIntInput(text):
    while True:
        try:
            value = int(input(text))
            return value
        except:
            print("Veuillez rentrer un nombre entier.", end='')
            continue

def mingMang(n):
    player = 1
    board = newBoard(n)
    isWinner = False
    while isWinner == False:
        displayPlayer(board, n, player)
        display(board, n)
        i, j = selectPawn(board, n, player)
        k, l = selectDestination(board, n, i, j)
        move(board, n, player, i, j, k, l)
        player = changePlayer(board, n, player)
        isWinner = lose(board, n, player)
        print("\n"*100)


    if isWinner == True:
        display(board, n)
        if player == 1:
            print("Le joueur ", end='')
            print("2", end='')
            print(" à gagné.")
        else:
            print("Le joueur ", end='')
            print("1", end='')
            print(" à gagné.")


initialisation()
