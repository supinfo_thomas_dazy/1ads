# mingMang project by Thomas Dazy (294828)
# Create in PYTHON 3.7
# rev 6.0

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# ATTENTION: ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 mingMang_BONUS.py'
# Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#

# Dans tous le programme: board[ligne][colone]

# Code couleur Joueur 1: 34
# Code couleur Joueur 2: 32
# Code couleur de réinitialisation: 0m

def newBoard(n):
    newBoard = []
    newRow = []

    for x in range(0, n, 1):
        newRow = []
        for y in range(0, n, 1):
            newRow.append("0")
        newBoard.append(newRow)
    for x in range(0, n, 1):
       newBoard[0][x] = "2"
    for x in range(0, n, 1):
       newBoard[x][0] = "1"
    for x in range(0, n, 1):
       newBoard[n-1][x] = "1"
    for x in range(0, n, 1):
       newBoard[x][n-1] = "2"
    return newBoard

def display(board, n):
    print("_"*((n*2)+3))
    for x in range(0, n, 1):
        print("| ", end='')
        for y in range(0, n, 1):
            if board[x][y] == "0":
                print(u"\u2022", end=' ')
            elif board[x][y] == "1":
                print(u"\u001b[0;34m", end='')
                print("X ", end='')
                print("\u001b[0m", end='')
            elif board[x][y] == "2":
                print(u"\u001b[0;32m", end='')
                print("O ", end='')
                print("\u001b[0m", end='')

        print("|")
    print("￣" * (n + 2))

def displayHighlight(board, n, a, b, player):
    print("\n"*100)
    displayPlayer(board, n, player)
    print("_"*((n*2)+3))
    for x in range(0, n, 1):
        print("| ", end='')
        for y in range(0, n, 1):
            if board[x][y] == "0":
                print(u"\u2022", end=' ')
            elif board[x][y] == "1":
                if x == a and y == b:
                    print(u"\u001b[34;47m", end='')
                else:
                    print(u"\u001b[0;34m", end='')
                print("X", end='')
                print("\u001b[0m ", end='')
            elif board[x][y] == "2":
                if x == a and y == b:
                    print(u"\u001b[32;47m", end='')
                else:
                    print(u"\u001b[0;32m", end='')
                print("O", end='')
                print("\u001b[0m ", end='')
        print("|")
    print("￣" * (n + 2))

def possiblePawn(board, n, player, i, j):
    # Si le pion séléctionné appartiens au joueur
    if i < n and j < n:
        if int(board[i][j]) == int(player):
            # On vérifie que le pion à de la place pour bouger
            # On vérifie qu'il y ai une case de libre à droite
            if j+1 != n and board[i][j+1] == "0":
                return True
            # On vérifie qu'il y ai une case de libre à gauche
            elif (j-1) >= 0 and board[i][j-1] == "0":
                return True
            # On vérifie qu'il y ai une case de libre en bas
            elif i+1 != n and board[i+1][j] == "0":
                return True
            # On vérifie qu'il y ai une case de libre en haut
            elif i-1 >= 0 and board[i-1][j] == "0":
                return True
            else:
                print(u"\u001b[1;31mLe pion n'a pas la place pour bouger")
                print("\u001b[0m", end='')
        else:
            print(u"\u001b[1;31mLe pion ne vous appartiens pas")
            print("\u001b[0m", end='')
    else:
        print(u"\u001b[1;31mSéléctionnez un pion sur le plateau")
        print("\u001b[0m", end='')

    return False


def selectPawn(board, n, player):
    toReturn = False
    while toReturn == False:
        i = int(userIntInput("Selectionnez une ligne")) - 1
        j = int(userIntInput("Selectionnez une colone")) - 1
        toReturn = possiblePawn(board, n, player, i, j)
    return i, j

def possibleDestination(board, n, i, j, k, l):
    # i et k horizontal
    # j et l vertical
    if i != k and j != l:
        print(u"\u001b[1;31mVous ne pouvez bouger que dans une seule direction")
        print("\u001b[0m", end='')
        return False
    if k >= n and l >= n:
        return False

    # Si les pions à déplacer sont sur la même case, on return False:
    if i == k and j == l:
        print(u"\u001b[1;31mVous devez déplacer un pion")
        print("\u001b[0m", end='')
        return False
    # Si les pions sont sur la même ligne: i et k sont égaux
    if i == k:
        # On vérifie que entre les pions il n'y ai rien:
        # Si on déplace le pion vers la droite:
        if j < l:
            for x in range(j+1, l+1, 1):

                if board[i][x] != "0":
                    print(u"\u001b[1;31mIl y a un pion qui bloque le chemin")
                    print("\u001b[0m", end='')
                    return False
        # Si on déplace le pion vers la gauche:
        elif j > l:
            for x in range(l, j, 1):
                if board[i][x] != "0":
                    print(u"\u001b[1;31mIl y a un pion qui bloque le chemin")
                    print("\u001b[0m", end='')
                    return False
    # Si les pions sont sur la même colone: j et l sont égaux
    if j == l:
        # On vérifie que entre les pions il n'y ai rien:
        # Si on déplace le pion vers le bas:
        if i < k:
            for x in range(i+1, k+1, 1):
                if board[x][j] != "0":
                    print(u"\u001b[1;31mIl y a un pion qui bloque le chemin")
                    print("\u001b[0m", end='')
                    return False
        # Si on déplace le pion vers le haut:
        if i > k:
            for x in range(k, i, 1):
                if board[x][j] != "0":
                    print(u"\u001b[1;31mIl y a un pion qui bloque le chemin")
                    print("\u001b[0m", end='')
                    return False
    return True

def selectDestination(board, n, i, j):
    toReturn = False
    while toReturn == False:
        k = int(userIntInput("Selectionnez une ligne de destination")) - 1
        l = int(userIntInput("Selectionnez une colone de destination")) - 1
        toReturn = possibleDestination(board, n, i, j, k, l)
    return k, l


def move(boardToMove, n, player, i, j, k, l):
    # On considère que l'on à testé si le pion pouvais se déplacer avec ces valeurs

    # 1. On bouge le pion:
    boardToMove[i][j] = "0"
    boardToMove[k][l] = str(player)

    # 2. On vérifie vers la droite si il y a des pions que l'on peut capturer
    m = l + 1
    while m < n and boardToMove[k][m] != str(player) and boardToMove[k][m] != "0" and m >= 0:
        m = m + 1
    if m >= 0 and m < n and boardToMove[k][m] == str(player):
        # CAPTURE VERS LA DROITE!!!
        m = l + 1
        while m < n and m >= 0 and boardToMove[k][m] != str(player) and boardToMove[k][m] != "0":
            boardToMove[k][m] = str(player)
            m = m + 1

    # 3. On vérifie vers la gauche si il y a des pions que l'on peut capturer
    m = l - 1
    while m < n and m >= 0 and boardToMove[k][m] != str(player) and boardToMove[k][m] != "0":
        m = m - 1
    if m >= 0 and m < n and boardToMove[k][m] == str(player):
        # CAPTURE VERS LA GAUCHE!!!
        m = l - 1
        while m < n and m >= 0 and boardToMove[k][m] != str(player) and boardToMove[k][m] != "0":
            boardToMove[k][m] = str(player)
            m = m - 1

    # 4. On vérifie vers le bas si il y a des pions que l'on peut capturer
    m = k + 1
    while m < n and m >= 0 and boardToMove[m][l] != str(player) and boardToMove[m][l] != "0":
        m = m + 1
        if m == 10:
            m = 9
    if m >= 0 and m < n and boardToMove[m][l] == str(player):
        # CAPTURE VERS LE BAS!!!
        m = k + 1
        while m < n and m >= 0 and boardToMove[m][l] != str(player) and boardToMove[m][l] != "0":
            boardToMove[m][l] = str(player)
            m = m + 1

    # 5. On vérifie vers le bas si il y a des pions que l'on peut capturer
    m = k - 1
    while m < n and m >= 0 and boardToMove[m][l] != str(player) and boardToMove[m][l] != "0":
        m = m - 1
    if m >= 0 and m < n and boardToMove[m][l] == str(player):
        # CAPTURE VERS LE HAUT!!!
        m = k - 1
        while m < n and m >= 0 and boardToMove[m][l] != str(player) and boardToMove[m][l] != "0":
            boardToMove[m][l] = str(player)
            m = m - 1

    return boardToMove


def lose(board, n, player):
    # Pour toutes les colones
    for x in range(0, n, 1):
        # Pour toutes les lignes
        for y in range(0, n, 1):
            # Si on a
            if board[x][y] == str(player):
                # On vérifie qu'il y ai une case de libre à droite
                if board[x][y+1] == "0" and y+1 != n:
                    return False
                # On vérifie qu'il y ai une case de libre à gauche
                if board[x][y-1] == "0" and (y-1) >= 0:
                    return False
                # On vérifie qu'il y ai une case de libre en bas
                if board[x+1][y] == "0" and x+1 != n:
                    return False
                # On vérifie qu'il y ai une case de libre en haut
                if board[x-1][y] == "0" and x-1 >= 0:
                    return False
    # Si on a trouvé aucuns pions qui peuvent être déplacés, on renvoie True
    return True

def displayPlayer(board, n, player):
    print("\u001b[0m", end='')
    print("Au tour du joueur ", end='')
    if player == 1:
        print(u"\u001b[1;34m1")
        print("\u001b[0m", end='')
    elif player == 2:
        print(u"\u001b[1;32m2")
        print("\u001b[0m", end='')

def changePlayer(board, n, player):
    if player == 1:
        return 2
    elif player == 2:
        return 1

def initialisation():
    print("\n"*100)
    print(u"\u001b[1;31m", end='')
    print("ATTENTION, ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 <nom_du_fichier>'")
    print("")
    print("Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci")
    print("\u001b[0m", end='\n')
    print(u"\u001b[1;33m", end='')
    n = int(userIntInput("Combiens de lignes et de colones voulez-vous ?"))
    print("\u001b[0m", end='')

    mingMang(n)

def saveGame(board, n, history):
    boardToSave = str(board[:])
    history.append(boardToSave)
    return history

def alreadyPlayed(board, n, history):
    if str(board) in history:
        return True
    else:
        return False


def userIntInput(text):
    while True:
        try:
            value = int(input(text))
            return value
        except:
            print(u"\u001b[1;31mVeuillez rentrer un nombre entier.", end='')
            print("\u001b[0m")
            continue


def mingMang(n):

    history = []
    player = 1
    board = newBoard(n)

    # On sauvegarde le plateau de jeu actuel
    history = saveGame(board, n, history)

    isWinner = False
    while isWinner == False:
        possibleMove = False
        while possibleMove == False:
            displayPlayer(board, n, player)
            display(board, n)
            i, j = selectPawn(board, n, player)
            displayHighlight(board, n, i, j, player)
            k, l = selectDestination(board, n, i, j)

            board = move(board, n, player, i, j, k, l)
            possibleMove = not alreadyPlayed(board, n, history)

            if possibleMove == False:
                print("\n"*100)
                print(u"\u001b[1;31mVous reviendriez à un état du jeu qui à déjà existé en effectuant ce déplacement")
                print("\u001b[0m", end='')
                # Si le déplacement n'est pas possible car on reviendrai à un plateau précédent, on refait le déplacement à l'envers
                board = move(board, n, player, k, l, i, j)

        player = changePlayer(board, n, player)

        # On sauvegarde le plateau de jeu actuel
        history = saveGame(board, n, history)

        isWinner = lose(board, n, player)
        print("\n"*100)

    if isWinner == True:
        display(board, n)
        if player == 1:
            print("Le joueur ", end='')
            print(u"\u001b[1;32m2", end='')
            print("\u001b[0m", end='')
            print(" à gagné.")
        else:
            print("Le joueur ", end='')
            print(u"\u001b[1;34m1", end='')
            print("\u001b[0m", end='')
            print(" à gagné.")


initialisation()
