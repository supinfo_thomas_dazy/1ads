# Nimble project by Thomas Dazy (294828)
# Created in PYTHON 3.7
# rev 1.5

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# ATTENTION: ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 nimble.py'
# Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#


# Import de la librairie permettant de générer le plateau de jeu
import random

# Dans le programme suivant:
    # "board" est une liste contenant le plateau de jeu actuel
    # n est un nombre (entier) contenant la taille du plateau de jeu
    # p est un nombre (entier) contenant la valeur maximale de pion par case à l'initialisation du plateau de jeu
    # i est un nombre entier positif
    # j est un nombre entier positif

    # Code couleur Joueur 1: 34
    # Code couleur Joueur 2: 32
    # Code couleur de réinitialisation: 0m

# Fonction qui génère un plateau de jeu ("board") de taille "n" avec au maximum "p" pions par cases
def newBoard(n, p):
    # On génère un plateau de jeu vide
    board = []
    # On ajoute "n" cases au plateau
    for x in range(0, n, 1):
        # On ajoute un nombre aléatoire de pions entre 0 et "p" dans la case actuelle
        randomNumber = random.randint(0, p)
        board.append(randomNumber)
    return board

# Fonction qui affiche le plateau de jeu
def display(board, n):
    print("| ", end='')
    for x in range(0, n, 1):
        if board[x] == 0:
            print(u"\u001b[1;32m0", end='')
            print("\u001b[0m", end='')
        elif board[x] <= 5:
            print(board[x], end='')
        else:
            print(u"\u001b[1;31m", end='')
            print(board[x], end='')
            print("\u001b[0m", end='')
        print(" | ", end='')
    print("")
    for x in range(0, n, 1):
        print("|---", end='')

        if board[x] > 9:
            print("-", end='')
        print("", end='')
    print("|")
    print("| ", end='')
    for x in range(1, n+1, 1):
        print(x, end='')
        if board[x-1] > 9:
            print(" ", end='')
        print(" | ", end='')
    print("")

def displayHighlight(board, n, i):
    print("| ", end='')
    for x in range(0, n, 1):
        if x == i - 1:
            print(u"\u001b[47m", end='')
        if board[x] == 0:
            print(u"\u001b[1;32m0", end='')
        elif board[x] <= 5:
            print(board[x], end='')
        else:
            print(u"\u001b[1;31m", end='')
            print(board[x], end='')
        print("\u001b[0m", end='')
        print(" | ", end='')
    print("")
    for x in range(0, n, 1):
        print("|---", end='')

        if board[x] > 9:
            print("-", end='')
        print("", end='')
    print("|")
    print("| ", end='')
    for x in range(1, n+1, 1):
        print(x, end='')
        if board[x-1] > 9:
            print(" ", end='')
        print(" | ", end='')
    print("")

# Fonction qui test si la case i contiens un pion qui peut bouger
# ATTENTION: i != 0, donc, quand on appelle la fonction, si pas déjà fait: i = i-1
def possibleSquare(board, n, i):
    if i - 1 <= n:
        if i - 1 > 0:
            if board[i - 1] > 0:
                if i != 0:
                    return True
    return False

# Fonction qui demande au joueur de sélectionner une case et vérifie si il y a un pion selectionnable (dans celle-ci)
def selectSquare(board, n):
    correctInput = False
    while correctInput == False:
        selectedSquare = userIntInput("Choisissez une case source : ")
        correctInput = possibleSquare(board, n, selectedSquare)
    return selectedSquare

# Fonction qui vérifie si la case séléctionnée peut être la destination du pion séléctionné
def possibleDestination(board, n, i, j):
    # i est une case qui contiens un pion sélectionnable
    # j est la case de destination
    if i > j:
        if i <= n:
            if j > 0:
                return True
    return False

# Fonction qui demande au joueur de selectionner une destination et vérifie si le pion peut s'y déplacer en fonction des règles du jeu
def selectDestination(board, n, i):
    correctInput = False
    while correctInput == False:
        selectedSquare = userIntInput("Choisissez une case destination : ")
        correctInput = possibleDestination(board, n, i, selectedSquare)
    return selectedSquare

# Fonction qui réalise le déplacement
def move(board, n, i, j):
    board[i - 1] = board[i - 1] - 1
    board[j - 1] = board[j - 1] + 1
    return board

# Fonction qui vérifie s'il reste un pion bougeable sur le plateau
def lose(board, n):
    if board[0] == 0:
        return False
    for x in range(1, n, 1):
        if board[x] != 0:
            return False
    return True

# Fonction qui affiche les messages d'avertissements et demande au joueur la configuration du plateau
def sizeOfTheBoard():
    print("\n"*100)
    print(u"\u001b[1;31m", end='')
    print("ATTENTION, ce programme n'a pas été conçu pour avoir un rendu correct avec PyCharm mais avec le terminal, merci de lancer ce programme avec le terminal en utilisant la commande 'python3 <nom_du_fichier>'")
    print("")
    print("Si vous lancez ce programme avec autre chose que le Terminal, merci de ne pas prende en compte l'affichage dans votre évaluation. Merci")
    print("\u001b[0m", end='\n')
    print(u"\u001b[1;33m", end='')
    print("Il est recommandé d'élargir la console")
    print(u"\u001b[0m", end='')
    n = userIntInput("Quel taille voulez-vous pour votre plateau ? ")
    p = userIntInput("Quel devrais être la plus grande valeur par cases possible ? ")
    return n, p

# Fonction qui change le joueur (si 1, alors 2 et si 2 alors 1)
def playerChange(currentPlayer):
    if currentPlayer == 1:
        return 2
    if currentPlayer == 2:
        return 1

# Procédure qui affiche le gagnant
def winnerDisplay(winnerPlayer, board):
    print("\n"*100)
    display(board, n)
    print("")
    if winnerPlayer == 2:
        print("Player ", end='')
        print(u"\u001b[0;34m1", end='')
        print(u"\u001b[0;32m WON", end='')
        print("\u001b[0m")
        print("Player ", end='')
        print(u"\u001b[0;32m2", end='')
        print(u"\u001b[0;31m LOST", end='')

    if winnerPlayer == 1:
        print("Player ", end='')
        print(u"\u001b[0;32m2", end='')
        print(u"\u001b[0;32m WON", end='')
        print("\u001b[0m")
        print("Player ", end='')
        print(u"\u001b[0;34m1", end='')
        print(u"\u001b[0;31m LOST", end='')

    print("\u001b[0m")

# Fonction qui s'occupe des entrées du joueur de manière à éviter tous crash
def userIntInput(text):
    while True:
        try:
            value = int(input(text))
            return value
        except:
            print(u"\u001b[1;31mVeuillez rentrer un nombre entier.", end='')
            print("\u001b[0m")
            continue

# Procédure qui génère la partie

def nimble(n, p):
    isWinner = False
    currentPlayer = 1
    board = newBoard(n, p)

    while isWinner == False:
        print("\n"*100)
        print("Player ", end='')
        if currentPlayer == 1:
            print(u"\u001b[1;34m1")
        else:
            print(u"\u001b[1;32m2")
        print("\u001b[0m", end='')
        display(board, n)

        # Sélection de la case source
        i = selectSquare(board, n)

        print("\n"*100)
        print("Player ", end='')
        if currentPlayer == 1:
            print(u"\u001b[1;34m1")
        else:
            print(u"\u001b[1;32m2")
        print("\u001b[0m", end='')
        displayHighlight(board, n, i)

        # Sélection de la case destination
        j = selectDestination(board, n, i)

        # We make the move
        board = move(board, n, i, j)
        currentPlayer = playerChange(currentPlayer)
        isWinner = lose(board, n)
    if isWinner == True:
        winnerDisplay(currentPlayer, board)

n, p = sizeOfTheBoard()
nimble(n, p)
