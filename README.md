![Python](https://img.shields.io/badge/python-3670A0?logo=python&logoColor=ffdd54&style=for-the-badge)

# 1ADS - Algorithm in Python

Cours, labs et projets de Python de 1ère année (A.Sc.1) à Supinfo en 2018 - 2019

Réalisé par Thomas Dazy

Ces projets sont Open source
