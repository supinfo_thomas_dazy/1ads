
def checkIfTrue(value, rowNum, liste):
    currentRowSum = 0

    for x in liste[rowNum]:
        currentRowSum += int(x)

    if value == currentRowSum:
        return "True"
    else:
        return "False"




valueToCheck = eval(input("Valeur ?"))
rowToCheck = eval(input("Ligne à vérifier ?"))
listeToCheck = [[6, 7, 2], [1, 5, 9], [8, 3, 4]]

if (checkIfTrue(valueToCheck, rowToCheck, listeToCheck)) == "True":
    print("Vrai !")
else:
    print("Faux !")