def checkIfCarreMagique(liste):
    rowSum = []
    colSum = []
    diagUpLeftToDownRight = 0
    diagUpRightToDownLeft = 0

    intColSum = 0
    intRowSum = 0

    listeSum = 0

    for x in range(0, len(liste), 1):
        # Pour chaque ligne de la liste
        currentRowSum = 0
        for value in liste[x]:
            currentRowSum += value
        rowSum.append(currentRowSum)

    for x in range(0, len(liste), 1):
        currentColSum = 0
        for y in range(0, len(liste), 1):
            currentColSum += liste[y][x]
        colSum.append(currentColSum)

    for x in range(0, len(liste), 1):
        diagUpLeftToDownRight += liste[x][x]

    for x in range(len(liste) - 1, -1, -1):
        diagUpRightToDownLeft += liste[x][x]

    for x in range(0, len(liste), 1):
        for y in range(0, len(liste), 1):
            listeSum += liste[x][y]

    for x in range(0, len(colSum), 1):
        intColSum += colSum[x]
    intColSum = intColSum / len(liste)

    for x in range(0, len(rowSum), 1):
        intRowSum += rowSum[x]
    intRowSum = intRowSum / len(liste)

    if intRowSum == intColSum and intColSum == diagUpLeftToDownRight and diagUpLeftToDownRight == diagUpRightToDownLeft and diagUpRightToDownLeft == int(listeSum / len(liste)):
        print("C'est un carré magique")
    else:
        print("Ce n'est pas un carré magique")

listeToCheck = [[6, 7, 2], [1, 5, 9], [8, 3, 4]]
checkIfCarreMagique(listeToCheck)