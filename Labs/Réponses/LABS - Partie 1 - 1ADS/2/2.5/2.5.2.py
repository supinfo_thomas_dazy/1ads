import random

nbrMatchs = eval(input("Saisissez un nombre impair d'allumettes"))
while nbrMatchs % 2 == 0:
    nbrMatchs = eval(input("ERREUR: saisissez un nombre IMPAIR d'allumettes"))

whosTurn = eval(input("Voulez vous commencer (1) ou que l'ordinateur commence (2)"))

lastMatchs = nbrMatchs

while lastMatchs > 0:
    if whosTurn == 1:
        print("Il vous reste", lastMatchs, "allumettes")
        print(lastMatchs*"| ")
        currentMatchsTaken = eval(input("Combiens d'allumettes voulez-vous prendre (1-3) ?"))
        while currentMatchsTaken < 1 or currentMatchsTaken > 3:
            currentMatchsTaken = eval(input("ERREUR, combiens d'allumettes voulez-vous prendre (1-3) ?"))

        lastMatchs -= currentMatchsTaken

        print("")

    elif whosTurn == 2:
        if lastMatchs >=4:
            currentMatchsTaken = random.randint(1, 3)
        elif lastMatchs > 1:
            currentMatchsTaken = lastMatchs - 1
        else:
            currentMatchTaken = 1
        print("L'ordinateur à pris", currentMatchsTaken, "allumettes")
        lastMatchs -= currentMatchsTaken

    if lastMatchs >= 1:
        if whosTurn == 1:
            whosTurn = 2
        else:
            whosTurn = 1


if lastMatchs <= 1:
    if whosTurn == 1:
        print("Vous avez perdu...")
    elif whosTurn == 2:
        print("Vous avez gagné")