a = eval(input("Combien de nombre parfaits voulez-vous ?"))
compteur = 0
compteurPerfect = 1
# est-ce que n est premier ?

from math import sqrt
from itertools import count, islice

def primeNumber(n):
    if n < 2:
        return False

    for number in islice(count(2), int(sqrt(n) - 1)):
        if n % number == 0:
            return False

    return True


while compteurPerfect <= a:
    if primeNumber((2**compteur)-1) == 1:
        print("Pour n =", compteur, "Le nombre", (2**(compteur-1))*((2**compteur)-1), "est parfait")
        compteurPerfect += 1
    compteur += 1
