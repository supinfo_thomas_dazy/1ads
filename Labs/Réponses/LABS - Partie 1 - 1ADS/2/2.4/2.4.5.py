# Suite de Conway:
# Pas fait par bibi

n = eval(input("Nombre de lignes de la suite de Conway ?"))

from itertools import groupby

def lookandsay(i):
    digits = str(i)
    newdigits = []
    for k, g in groupby(digits):
        newdigits += [sum(1 for _ in g), k]
    return int("".join(map(str, newdigits)))

def conway(i):
    yield i
    while True:
        i = lookandsay(i)
        yield i

cw01 = conway(1)
for _ in range(n):
    print(" ".join(str(next(cw01))))