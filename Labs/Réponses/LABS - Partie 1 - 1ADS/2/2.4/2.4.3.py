wordOne = input("Premier mot ?")
wordTwo = input("Second mot ?")

if len(wordOne) != len(wordTwo):
    print("Les deux mots n'on pas la même longueur...")
else:
    hammingLenght = 0

    for x in range(0, len(wordOne), 1):
        if wordOne[x] != wordTwo[x]:
            hammingLenght += 1

    if hammingLenght >0:
        print("La distande de Hamming est de:", hammingLenght)
    else:
        print("Il n'y a pas de distance de Hamming.")