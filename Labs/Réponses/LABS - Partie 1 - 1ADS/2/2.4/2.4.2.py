Uzero = eval(input("Entrez la valeur initiale de la suite de Syracuse"))
n = eval(input("Nombre d'itération de la suite ?"))

Un = Uzero

print("U 0 =", Uzero)
for x in range(1, n+1, 1):
    if Un % 2 == 0:
        # Si Un est pair:
        UnPlusOne = Un / 2
    else:
        # Si Un est impair:
        UnPlusOne = 3 * Un + 1

    print("U", x, "=", int(UnPlusOne))
    Un = UnPlusOne