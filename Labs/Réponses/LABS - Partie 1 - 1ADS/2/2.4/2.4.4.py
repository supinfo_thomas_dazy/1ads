wordOne = input("Premier mot ?")
wordTwo = input("Second mot ?")

# Si à la fin du programme, isAnagramme est = à 1, alors les deux mots sont des annagrammes
isAnagrammes = 1

import string
list = []
for c in string.ascii_lowercase:
    list.append(c)
    if wordOne.count(c) != wordTwo.count(c):
        isAnagrammes = 0
if len(wordOne) != len(wordTwo):
    isAnagrammes = 0
if isAnagrammes == 1:
    print("Les deux mots sont des anagrammes")
else:
    print("Les deux mots ne sont pas des anagrammes")