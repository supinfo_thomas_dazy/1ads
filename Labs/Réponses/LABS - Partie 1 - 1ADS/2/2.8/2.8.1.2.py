
def checkIfThereIsA0(listeToCheck):
    for x in range(0, len(listeToCheck), 1):
        for y in range(0, len(listeToCheck), 1):
            if listeToCheck[x][y] == 0:
                return "True"

    return "False"


currentPlate = [[0, 1, 2], [2, 1, 0], [2, 0, 1]]

print(checkIfThereIsA0(currentPlate))