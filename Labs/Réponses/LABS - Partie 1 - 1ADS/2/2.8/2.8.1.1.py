def changeCaracter(caracter):
    if caracter == 0:
        return "•"
    elif caracter == 1:
        return "X"
    else:
        return "O"

def printGamePlate(listeToShow):
    print(" _______")
    print("|", changeCaracter(listeToShow[0][0]), changeCaracter(listeToShow[0][1]), changeCaracter(listeToShow[0][2]), "|")
    print("|", changeCaracter(listeToShow[1][0]), changeCaracter(listeToShow[1][1]), changeCaracter(listeToShow[1][2]),"|")
    print("|", changeCaracter(listeToShow[2][0]), changeCaracter(listeToShow[2][1]), changeCaracter(listeToShow[2][2]), "|")
    print(" -------")


currentPlate = [[0, 1, 2], [2, 1, 0], [2, 0, 1]]

printGamePlate(currentPlate)