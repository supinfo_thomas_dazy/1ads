def checkIfWinner(plateToCheck):

    firstRow = plateToCheck[0]
    secondRow = plateToCheck[1]
    thirdRow = plateToCheck[2]

    if firstRow[0] == firstRow[1] and firstRow[1] == firstRow[2] and firstRow[0] != 0:
        return firstRow[0]
    if secondRow[0] == secondRow[1] and secondRow[1] == secondRow[2] and secondRow[0] != 0:
        return secondRow[0]
    if thirdRow[0] == thirdRow[1] and thirdRow[1] == thirdRow[2] and thirdRow[0] != 0:
        return thirdRow[0]

    if firstRow[0] == secondRow[0] and secondRow[0] == thirdRow[0] and firstRow[0] != 0:
        return firstRow[0]
    if firstRow[1] == secondRow[1] and secondRow[1] == thirdRow[1] and firstRow[1] != 0:
        return firstRow[1]
    if firstRow[2] == secondRow[2] and secondRow[2] == thirdRow[2] and firstRow[2] != 0:
        return firstRow[2]

    if firstRow[0] == secondRow[1] and secondRow[1] == thirdRow[2] and firstRow[0] != 0:
        return firstRow[0]
    if firstRow[2] == secondRow[1] and secondRow[1] == thirdRow[0] and firstRow[2] != 0:
        return firstRow[2]

    return 0

currentPlate = [[1, 0, 2], [2, 1, 0], [2, 0, 1]]

print("Winner: Player", checkIfWinner(currentPlate))