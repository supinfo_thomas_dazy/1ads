# Morpion by Thomas Dazy (c) 2018

def changeCaracter(caracter):
    if caracter == 0:
        return "•"
    elif caracter == 1:
        return "X"
    else:
        return "O"

def printGamePlate(listeToShow):
    print(" _______")
    print("|", changeCaracter(listeToShow[0][0]), changeCaracter(listeToShow[0][1]), changeCaracter(listeToShow[0][2]), "|")
    print("|", changeCaracter(listeToShow[1][0]), changeCaracter(listeToShow[1][1]), changeCaracter(listeToShow[1][2]),"|")
    print("|", changeCaracter(listeToShow[2][0]), changeCaracter(listeToShow[2][1]), changeCaracter(listeToShow[2][2]), "|")
    print(" -------")

def checkIfThereIsA0(listeToCheck):
    for x in range(0, len(listeToCheck), 1):
        for y in range(0, len(listeToCheck), 1):
            if listeToCheck[x][y] == 0:
                return "True"
    return "False"

def changePiece(valueX, valueY, listeToChange, currentPlayerNumber):
    if currentPlayerNumber > 0 and currentPlayerNumber <= 2:
        if listeToChange[valueX][valueY] == 0:
            listeToChange[valueX][valueY] = currentPlayerNumber
            return listeToChange
        else:
            return "0"
    else:
        return "0"


def checkIfWinner(plateToCheck):

    firstRow = plateToCheck[0]
    secondRow = plateToCheck[1]
    thirdRow = plateToCheck[2]

    if firstRow[0] == firstRow[1] and firstRow[1] == firstRow[2] and firstRow[0] != 0:
        return firstRow[0]
    if secondRow[0] == secondRow[1] and secondRow[1] == secondRow[2] and secondRow[0] != 0:
        return secondRow[0]
    if thirdRow[0] == thirdRow[1] and thirdRow[1] == thirdRow[2] and thirdRow[0] != 0:
        return thirdRow[0]

    if firstRow[0] == secondRow[0] and secondRow[0] == thirdRow[0] and firstRow[0] != 0:
        return firstRow[0]
    if firstRow[1] == secondRow[1] and secondRow[1] == thirdRow[1] and firstRow[1] != 0:
        return firstRow[1]
    if firstRow[2] == secondRow[2] and secondRow[2] == thirdRow[2] and firstRow[2] != 0:
        return firstRow[2]

    if firstRow[0] == secondRow[1] and secondRow[1] == thirdRow[2] and firstRow[0] != 0:
        return firstRow[0]
    if firstRow[2] == secondRow[1] and secondRow[1] == thirdRow[0] and firstRow[2] != 0:
        return firstRow[2]

    return 0

def initialisation():
    currentPlate = [[0,0,0],[0,0,0],[0,0,0]]
    return currentPlate

def intervertPlayers(player):
    if player == 1:
        player = 2
    elif player == 2:
        player = 1
    return player


# Initialisation du plateau de jeux:
currentPlate = initialisation()
currentPlayer = 1
isWinner = 0

print("Morpion ((c) Thomas Dazy 2018)")

while isWinner == 0:
    print("\n" * 100)
    print("Au tour du joueur", currentPlayer)
    print("")
    printGamePlate(currentPlate)
    print("")
    print("Où poser ton blaze ? (x puis y)")
    coordonateY = eval(input("Coordonnées horizontales ?"))-1
    coordonateX = eval(input("Coordonnées verticales ?"))-1

    isPieceChanged = changePiece(int(coordonateX), int(coordonateY), currentPlate, currentPlayer)

    while isPieceChanged[0][0] == "0":
        print("ERROR: Cette case à déjà été claim...")
        # Il est normal que les coodronnées soient inversées..
        coordonateY = eval(input("Coordonnées horizontales ?")) - 1
        coordonateX = eval(input("Coordonnées verticales ?")) - 1

        isPieceChanged = changePiece(int(coordonateX), int(coordonateY), currentPlate, currentPlayer)

    currentPlate = isPieceChanged

    currentPlayer = intervertPlayers(currentPlayer)

    #print(currentPlate)

    isWinner = checkIfWinner(currentPlate)


if isWinner > 0:
    if isWinner == 1:
        print("")
        printGamePlate(currentPlate)
        print("")
        print("Bravo à toi joueur 1, le clan de la croix conquérira le monde!!!")
    elif isWinner == 2:
        print("Brabo à toi joueur 2, des petits trous, des petits trous, toujours des petits trous")

    print("THE END")
    print("")
    print("************CREDITS************")
    print("*****CREATOR : Thomas Dazy*****")
    print("********COPYRIGHTS 2018********")
    print("*******************************")