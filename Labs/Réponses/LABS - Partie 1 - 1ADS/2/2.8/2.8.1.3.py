
def changePiece(valueX, valueY, listeToChange, currentPlayerNumber):
    if currentPlayerNumber > 0 and currentPlayerNumber <= 2:
        listeToChange[valueX][valueY] = currentPlayerNumber
    return listeToChange

currentPlate = [[0, 1, 2], [2, 1, 0], [2, 0, 1]]
print(changePiece(0, 0, currentPlate, 1))