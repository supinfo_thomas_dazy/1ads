nbrRow = abs(eval(input("Nombre de ligne ?")))

for x in range(1, nbrRow*2+1, 2):
    currentRow = x
    currentFreeSpaceRow = (nbrRow * 2 - x) // 2

    while (currentFreeSpaceRow > 0):
        print(" ", end="")
        currentFreeSpaceRow = currentFreeSpaceRow - 1

    while(currentRow > 0):
        print("*", end="")
        currentRow = currentRow - 1

    print(" ")