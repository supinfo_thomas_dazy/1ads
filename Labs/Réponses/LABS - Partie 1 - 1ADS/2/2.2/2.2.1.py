inValue = eval(input("Quelle somme ?"))

# print(inValue)

if inValue < 100:
    print("La valeur doit être suppérieure à 100€")

if inValue >= 100 and inValue <= 500:
    print("La valeur est correcte, calcul en cours")

    endValue = inValue * 0.95

    print("La valeur finale avec la remise de 5% est :", endValue)

if inValue > 500:
    print("La valeur est correcte, calcul en cours")

    endValue = inValue * 0.92

    print("La valeur finale avec la remise de 8% est :", endValue)