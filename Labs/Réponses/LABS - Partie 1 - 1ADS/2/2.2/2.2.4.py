x = eval(input("La valeur de x"))
y = eval(input("La valeur de y"))

if x < 0:
    if y < 0:
        print("La valeur finale sera positive")
    else:
        print("La valeur finale sera négative")

elif y < 0:
    if x < 0:
        print("La valeur finale sera positive")
    else:
        print("La valeur finale sera négative")

elif x >= 0 and y >= 0:
    print("La valeur finale sera positive")