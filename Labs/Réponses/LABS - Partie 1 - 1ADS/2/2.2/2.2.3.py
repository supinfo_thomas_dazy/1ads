x = eval(input("La valeur de x ?"))
y = eval(input("La valeur de y ?"))
z = eval(input("La valeur de z ?"))

if x == y and y == z:
    print("x = y = z")

elif x <= y and x <= z:
    # x est le plus petit
    if y <= z:
        print("x <= y <= z")
    else:
        print("x <= z <= y")

elif y <= x and y <= z:
    # y est le plus petit
    if x <= z:
        print("y <= x <= z")
    else:
        print("y <= z <= x")

elif z <= x and z <= y:
    # z est le plus petit
    if x <= y:
        print("z <= x <= y")
    else:
        print("z <= y <= x")

elif x == y:
    if x < z:
        print("x = y < z")
    else:
        print("x = y > z")

elif x == z:
    if x < y:
        print("x = z < y")
    else:
        print("x = z > y")

elif y == z:
    if y < x:
        print("y = z < x")
    else:
        print("y = z > x")


else:
    print("ERROR")