liste = ['1', '2', '2', '3', '4', '4', '4', 'toto', 'test']

print("La liste contient:", liste)
print("")

charToFind = input("Quel est l'élément dont vous voulez compter le nombre d'occurence dans la liste ?")

if liste.count(charToFind) == 0:
    print("Cet élément n'existe pas dans la liste")
    print("Son index est: -1")
else:
    print("Cet élément apparait", liste.count(charToFind), "fois dans la liste")
    print("Son index est:", liste.index(charToFind))