liste = [12, 23, 55, 11, 7, 64, 3]

smallestItem = liste[1]

for x in range(0, len(liste), 1):
    if liste[x] < smallestItem:
        smallestItem = liste[x]

print("Le plus petit Item de la liste est:", smallestItem, "et son indice est:", liste.index(smallestItem))