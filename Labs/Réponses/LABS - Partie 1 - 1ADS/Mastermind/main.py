# Librarie utilisée pour générer des nombres aléatoires
import random

# Images en cas de gain ou de perte
def looserPicture():
    print("         _             _            _            _            _            _ ")
    print("        _\ \          /\ \         /\ \         / /\         /\ \         /\ \ ")
    print("       /\__ \        /  \ \       /  \ \       / /  \       /  \ \       /  \ \ ")
    print("      / /_ \_\      / /\ \ \     / /\ \ \     / / /\ \__   / /\ \ \     / /\ \ \ ")
    print("     / / /\/_/     / / /\ \ \   / / /\ \ \   / / /\ \___\ / / /\ \_\   / / /\ \_ \ ")
    print("    / / /         / / /  \ \_\ / / /  \ \_\  \ \ \ \/___// /_/_ \/_/  / / /_/ / / ")
    print("   / / /         / / /   / / // / /   / / /   \ \ \     / /____/\    / / /__\/ / ")
    print("  / / / ____    / / /   / / // / /   / / /_    \ \ \   / /\____\/   / / /_____/ ")
    print(" / /_/_/ ___/\ / / /___/ / // / /___/ / //_/\__/ / /  / / /______  / / /\ \ \     ")
    print("/_______/\__\// / /____\/ // / /____\/ / \ \/___/ /  / / /_______\/ / /  \ \ \    ")
    print("\_______\/    \/_________/ \/_________/   \_____\/   \/__________/\/_/    \_\/ ")

def winnerPicture():
    print("        _              _          _             _             _            _      ")
    print("       / /\      _    /\ \       /\ \     _    /\ \     _    /\ \         /\ \ ")
    print("      / / /    / /\   \ \ \     /  \ \   /\_\ /  \ \   /\_\ /  \ \       /  \ \ ")
    print("     / / /    / / /   /\ \_\   / /\ \ \_/ / // /\ \ \_/ / // /\ \ \     / /\ \ \ ")
    print("    / / /_   / / /   / /\/_/  / / /\ \___/ // / /\ \___/ // / /\ \_\   / / /\ \_ \ ")
    print("   / /_//_/\/ / /   / / /    / / /  \/____// / /  \/____// /_/_ \/_/  / / /_/ / / ")
    print("  / _______/\/ /   / / /    / / /    / / // / /    / / // /____/\    / / /__\/ / ")
    print(" / /  \____\  /   / / /    / / /    / / // / /    / / // /\____\/   / / /_____/   ")
    print("/_/ /\ \ /\ \/___/ / /__  / / /    / / // / /    / / // / /______  / / /\ \ \     ")
    print("\_\//_/ /_/ //\__\/_/___\/ / /    / / // / /    / / // / /_______\/ / /  \ \ \ ")
    print("    \_\/\_\/ \/_________/\/_/     \/_/ \/_/     \/_/ \/__________/\/_/    \_\/ ")





# Cette fonction créer un liste de 4 chiffre aléatoire compris entre 1 et 6
def combinaison():
    liste = []
    for i in range(4):
        liste.append(random.randint(1, 6))
    return liste


# Cette fonction demande au joueur de rentrer une séquence
def joueurJoue():
    erreur = 1
    while erreur != 0:
        liste = int(input("Entre une combinaison de 4 chiffres entre 1 et 6:"))
        listeEcrit = str(liste)
        erreur = 0
        if len(listeEcrit) == 4:
            for i in range(4):
                if int(listeEcrit[i]) < 1 or int(listeEcrit[i]) > 6:
                    erreur += 1
        else:
            erreur += 1
    liste = []
    for i in range(4):
        liste.append(int(listeEcrit[i]))
    return liste


def joueurGagne(combinaisonGagnante, combinaisonJoueur):
    erreur = 0
    for i, x in enumerate(combinaisonGagnante):
        if combinaisonGagnante[i] != combinaisonJoueur[i]:
            erreur += 1
    if erreur != 0:
        return False
    else:
        return True

#Génère le plateau de jeux
def generateGamePlate(currentGame, currentGameRowAnswer):
    print("___________________")
    for x in range (0, len(currentGame), 1):
        print("|", currentGame[x][0], currentGame[x][1], currentGame[x][2], currentGame[x][3], "¦", currentGameRowAnswer[x][0], "※", currentGameRowAnswer[x][1], "|", )
    print("___________________")

# Initialisation du jeu
def initialisation():
    currentGame = []
    currentIteration = 1
    isWinner = False
    currentGameRowAnswer = [[0, 0]]
    return currentGame, currentIteration, isWinner, currentGameRowAnswer

# Première fonction permettant de vérifier les différentes lignes par rapport à la combinaison
def checkIfGoodAndPlaced(currentGame, answer):
   goodAndPlaced = 0
   goodAndPlacedList = []
   for x in range(0, len(currentGame), 1):
       for y in range(0, 4, 1):
           if currentGame[x][y] == answer[y]:
               goodAndPlaced += 1
       goodAndPlacedList.append(goodAndPlaced)
       goodAndPlaced = 0

   return goodAndPlacedList

# Deuxième fonction permettant de vérifier les différentes lignes par rapport à la combinaison
def checkIfGoodButNotPlaced(currentGame, answer):
    goodAndPlacedList = checkIfGoodAndPlaced(currentGame, answer)
    alreadyFound = []
    goodAndMaybePlaced = []

    for x in range(0, len(currentGame), 1):
        goodAndMaybePlacedInCurrentRow = 0
        for y in range(1, 7, 1):
            numberInGame = currentGame[x].count(y)
            numberInAnswer = answer.count(y)
            if numberInAnswer <= numberInGame:
                goodAndMaybePlacedInCurrentRow = goodAndMaybePlacedInCurrentRow + numberInAnswer
            else:
                goodAndMaybePlacedInCurrentRow = goodAndMaybePlacedInCurrentRow + numberInGame
        goodAndMaybePlaced.append(goodAndMaybePlacedInCurrentRow)




    return goodAndMaybePlaced, goodAndPlacedList


# Dernière fonction permettant de vérifier les différentes lignes par rapport à la combinaison
def checkGame(currentGame, answer):
    finalList = []
    goodAndMaybePlaced, goodAndPlacedList = checkIfGoodButNotPlaced(currentGame, answer)
    for x in range(0, len(goodAndMaybePlaced)):
        goodAndBadPlaced = goodAndMaybePlaced[x] - goodAndPlacedList[x]
        intermediateList = [goodAndBadPlaced, goodAndPlacedList[x]]
        finalList.append(intermediateList)
    return finalList

# Vérifie s'il y a un gagnant
def isWinner(combinaison, currentGameRowAnswer):
    for x in range(0, len(currentGameRowAnswer), 1):
        if currentGameRowAnswer[x][1] == combinaison:
            return 1
        else:
            return 0



# Partie principale du jeu:

# Initialisation
currentGame, currentIteration, isWinner, currentGameRowAnswer = initialisation()
combinaison = combinaison()

# Affichage du début
print('\n' * 100)
print("")
print("Bienvenue dans le Mastermind!!!")
print("")
print("Dans les deux colones de droite, la 1ère est le nombre de caractères juste mais mal placer et la deuxième est le nombre de bon caractères bien placés")
print("Tu as 12 essais")
print("")
print("")

# Boucle de jeu principale
while isWinner == False and currentIteration <= 13:
    generateGamePlate(currentGame, currentGameRowAnswer)
    print("Essais", currentIteration - 1)
    playerGame = joueurJoue()
    currentGame.append(playerGame)
    currentGameRowAnswer = checkGame(currentGame, combinaison)
    currentIteration += 1
    print('\n' * 100)
    if playerGame == combinaison:
        isWinner = True

# Si il y a un gagnant
if isWinner == True:
    winnerPicture()
    print("")
    print("Félicitation, tu as trouve le code en moins de 12 essais ;)")
elif currentIteration >= 12:
    looserPicture()
    print("")
    print("Dommage, tout le monde ne naît pas intéligent... le code était:", combinaison[0], combinaison[1], combinaison[2], combinaison[3])