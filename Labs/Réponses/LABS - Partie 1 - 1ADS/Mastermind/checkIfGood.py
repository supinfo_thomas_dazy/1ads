


def checkIfGoodAndPlaced(currentGame, answer):
   goodAndPlaced = 0
   goodAndPlacedList = []
   for x in range(0, len(currentGame), 1):
       for y in range(0, 4, 1):
           if currentGame[x][y] == answer[y]:
               goodAndPlaced += 1
       goodAndPlacedList.append(goodAndPlaced)
       goodAndPlaced = 0

   return goodAndPlacedList


def checkIfGoodButNotPlaced(currentGame, answer):
    goodAndPlacedList = checkIfGoodAndPlaced(currentGame, answer)
    alreadyFound = []
    goodAndMaybePlaced = []

    for x in range(0, len(currentGame), 1):
        goodAndMaybePlacedInCurrentRow = 0
        for y in range(1, 7, 1):
            numberInGame = currentGame[x].count(y)
            numberInAnswer = answer.count(y)
            if numberInAnswer <= numberInGame:
                goodAndMaybePlacedInCurrentRow = goodAndMaybePlacedInCurrentRow + numberInAnswer
            else:
                goodAndMaybePlacedInCurrentRow = goodAndMaybePlacedInCurrentRow + numberInGame
        goodAndMaybePlaced.append(goodAndMaybePlacedInCurrentRow)




    return goodAndMaybePlaced, goodAndPlacedList



def checkGame(currentGame, answer):
    finalList = []
    goodAndMaybePlaced, goodAndPlacedList = checkIfGoodButNotPlaced(currentGame, answer)
    for x in range(0, len(goodAndMaybePlaced)):
        goodAndBadPlaced = goodAndMaybePlaced[x] - goodAndPlacedList[x]
        intermediateList = [goodAndBadPlaced, goodAndPlacedList[x]]
        finalList.append(intermediateList)
    return finalList



currentGame = [[1, 2, 3, 4], [3, 2, 4, 5], [5, 5, 3, 5], [6, 6, 6, 6]]
answer = [3, 2, 4, 5]
print(checkGame(currentGame, answer))