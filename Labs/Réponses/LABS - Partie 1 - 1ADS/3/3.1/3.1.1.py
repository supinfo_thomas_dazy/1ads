n = eval(input("Entrez la n ème valeur de la suite de fibonacci qui sera calculée"))

fMinusOne = 1
fMinusTwo = 0
iteration = 1


def recursiveFibonacci(lastValue, lastLastValue, iteration, n):
    if iteration == n - 1:
        return lastValue + lastLastValue
    else:
        newValue = lastLastValue + lastValue
        iteration += 1
        return recursiveFibonacci(newValue, lastValue, iteration, n)



if n == 0:
    print("F 0: 0")

if n == 1:
    print("F 1: 1")
if n >= 2:
    print("F 0: 0")
    print("F 1: 1")



print(recursiveFibonacci(fMinusOne, fMinusTwo, iteration, n))