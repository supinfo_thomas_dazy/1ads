# Suite de syracuse:
# Pour tout n >= 1, U(n+1)=
# U(n) / 2 si U(n) est pair
# 3 * U(n) + 1 si U(n) est impair



Uzero = eval(input("Entrez la valeur initiale de la suite de Syracuse"))
n = eval(input("Nombre d'itération de la suite ?"))

Un = Uzero
lastValue = Uzero
iteration = 1

print("U 0 =", Uzero)

def syracuse(Uzero, n, iteration, lastValue):
    if iteration == n:
        return lastValue
    else:
        if lastValue % 2 == 0:
            # Si Un est pair:
            lastValue = lastValue / 2
        else:
            # Si Un est impair:
            lastValue = 3 * lastValue + 1

        iteration += 1
        return syracuse(Uzero, n, iteration, lastValue)

print("Résultat: ", syracuse(Uzero, n, iteration, lastValue))