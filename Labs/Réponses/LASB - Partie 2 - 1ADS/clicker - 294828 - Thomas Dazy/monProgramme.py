import pygame, sys
import time
import random
from pygame.locals import *
pygame.init()


maSurface = pygame.display.set_mode((500,300))
pygame.display.set_caption('J\'ai gagné 0,25 pts!!!')

WHITE = (255,255,255)
BLACK = (0,0,0)


def changeScore(newScore):
    # Rectangle contenant le score
    pygame.draw.rect(maSurface,WHITE,(450,0,100,19))

    fontObj = pygame.font.Font('freesansbold.ttf',18)
    texteSurface = fontObj.render(str(newScore),True,BLACK,WHITE)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (450,0)
    maSurface.blit(texteSurface,texteRect)

def changePosition(score):
    nouvellePositionX = random.randint(0,400)
    nouvellePositionY = random.randint(0, 220)
    deletePicture(score)
    maSurface.blit(monImage,(nouvellePositionX,nouvellePositionY))
    return nouvellePositionX, nouvellePositionY
    fond = pygame.image.load("fond.jpg")
    maSurface.blit(fond,(0,0))
    changeScore(score)

    pygame.display.update()

def deletePicture(score):
    # maSurface.blit(monImage,(5000,5000))
    fond = pygame.image.load("fond.jpg")
    maSurface.blit(fond,(0,0))
    changeScore(score)


def initialisation(score):
    # Image de fond
    fond = pygame.image.load("fond.jpg")
    maSurface.blit(fond,(0,0))
    changeScore(0)
    positionX, positionY = changePosition(score)
    return positionX, positionY

score = 0
sec = time.time()
tempsDepart = sec
monImage = pygame.image.load('avion.png')
positionX, positionY = initialisation(score)

inProgress = True
while inProgress:
    sec = time.time()
    for event in pygame.event.get():
        if event.type == QUIT:
            inProgress = False

        if event.type == MOUSEBUTTONUP:
            poseX, poseY = event.pos
            if poseX >= positionX and poseX <= (positionX+80) and poseY >= positionY and poseY <= (positionY+80):
                score = score+1
                positionX, positionY = 5000, 5000
                changeScore(score)
                deletePicture(score)

    waitingTime = random.randint(3,6)
    waitingTime = waitingTime/10
    if tempsDepart + waitingTime < sec:
        positionX, positionY = changePosition(score)
        tempsDepart = sec


    pygame.display.update()
pygame.quit()
