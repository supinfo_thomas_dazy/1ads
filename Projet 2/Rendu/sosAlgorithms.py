# SOS Game, code par Thomas Dazy et Damien SCHWARZROCK (c)
# 2018 (c)
# V1.0





###### Les différentes variables: ######
# • « board » : liste à deux dimensions d’entiers égaux à 0, 1 ou 2 représentant le plateau de jeu.
# • « n » : entier strictement positif égal au nombre de lignes et colonnes de « board ».
# • « player » : entier représentant le joueur dont c’est le tour.
# • « scores » : liste de deux entiers représentant les scores des deux joueurs.
# • « i » : entier quelconque.
# • « j » : entier quelconque.
# • « lines » : liste à deux dimensions dont les éléments sont des listes de deux tuples de deux
#   entiers. Ces listes de deux tuples correspondent aux coordonnées de deux points, les
#   extrémités d’une ligne lors d’un alignement.
# • «l» : entier valant 1 ou 2, représentant un «S» ou un «O».

### S = 1; O = 2
########################################





# Une fonction « newBoard(n) » qui retourne une liste à deux dimensions représentant l’état initial d’un plateau de jeu à n lignes et n colonnes.
def newBoard(n):
    newBoard = []
    newRow = []

    for x in range (0, n, 1):
        newRow = []
        for y in range (0, n, 1):
            newRow.append("0")
        newBoard.append(newRow)
    return newBoard


# Une fonction « possibleSquare(board,n,i,j) » qui retourne True si i et j sont les coordonnées d’une case où un joueur peut poser une lettre, et False sinon.
def possibleSquare(board, n, i, j):
    if board[i][j] == "0":
        return True
    else:
        return False
    return False

# Une procédure « updateScoreS(board,n,i,j,scores,player,lines) » qui suppose que le joueur player ait posé la lettre « S » sur la case de coordonnées i et j. Elle recherche alors les éventuels alignements de « SOS » que cela a pu engendrer, et met à jour le score du joueur player et la liste lines.
def updateScoreS(board, n, i, j, scores, player, lines):
    # print("i: ", i, " j: ", j)
    # Vérification vers le haut de l'alignement d'un SOS:
    if i > 1:   # On vérifie qu'il y ai la place au dessus pour écrire SOS
        if board[i-1][j] == 2 and board[i-2][j] == 1:
            newInLines = [i, j, i-2, j]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers le haut droite de l'alignement d'un SOS:
    if i > 1 and j < n-2:
        if board[i-1][j+1] == 2 and board[i-2][j+2] == 1:
            newInLines = [i, j, i-2, j+2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers la droite de l'alignement d'un SOS:
    if j < n-2:
        if board[i][j+1] == 2 and board[i][j+2] == 1:
            newInLines = [i, j, i, j+2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers bas-droite de l'alignement d'un SOS:
    if i < n-2 and j < n-2:
        if board[i+1][j+1] == 2 and board[i+2][j+2] == 1:
            newInLines = [i, j, i+2, j+2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers le bas de l'alignement d'un SOS:
    if i < n-2:
        if board[i+1][j] == 2 and board[i+2][j] == 1:
            newInLines = [i, j, i+2, j]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers le bas-gauche de l'alignement d'un SOS:
    if i < n-2 and j > 1:
        if board[i+1][j-1] == 2 and board[i+2][j-2] == 1:
            newInLines = [i, j, i+2, j-2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers la gauche de l'alignement d'un SOS:
    if j > 1:
        if board[i][j-1] == 2 and board[i][j-2] == 1:
            newInLines = [i, j, i, j-2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    # Vérification vers la gauche-haut de l'alignement d'un SOS:
    if i > 1 and j > 1:
        if board[i-1][j-1] == 2 and board[i-2][j-2] == 1:
            newInLines = [i, j, i-2, j-2]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1                                 # On ajoute 1 au score du joueur actuel

    return scores, lines

# Une procédure « updateScoreO(board,n,i,j,scores,player,lines) » qui suppose que le joueur player ait posé la lettre « O » sur la case de coordonnées i et j. Elle recherche alors les éventuels alignements de « SOS » que cela a pu engendrer, et met à jour le score du joueur player et la liste lines.
def updateScoreO(board, n, i, j, scores, player, lines):
    if i >= 1 and j >= 0 and i < n-1 and j < n:                                 # On vérifie que l'on ai la place autour pour composer "SOS"
        # On vérifie à la verticale
        if board[i-1][j] == 1 and board[i+1][j] == 1:
            newInLines = [i-1, j, i+1, j]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1
    if i >= 0 and j >= 1 and i < n and j < n-1:
        # On vérifie à l'horizontale:
        if board[i][j-1] == 1 and board[i][j+1] == 1:
            newInLines = [i, j-1, i, j+1]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1

    if i >= 1 and j >= 1 and i < n - 1 and j < n - 1:
        # On vérifie la diagonale de en haut à droite à en bas à gauche:
        if board[i-1][j+1] == 1 and board[i+1][j-1] == 1:
            newInLines = [i-1, j+1, i+1, j-1]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1
        # On vérifie la diagonale de en haut à gauche jusqu'à en bas à droite:
        if board[i-1][j-1] == 1 and board[i+1][j+1] == 1:
            newInLines = [i-1, j-1, i+1, j+1]
            lines.append(newInLines)
            scores[player - 1] = scores[player - 1] + 1

    return scores, lines



# Une procédure « update(board,n,i,j,l,scores,player,lines) » qui commence par mettre à jour le plateau de jeu en affectant la valeur l à la case de coordonnées i et j. Selon les cas elle appelle ensuite l’une des deux procédures précédentes. Lors de l’appel de cette procédure, la liste lines est vide.
def update(board, n, i, j, l, scores, player, lines):
    newLines = []
    # Ici on a déjà vérifié si on pouvais mettre une lettre à cet emplacement
    board[i][j] = l
    # On s'occupe de la modification des scores en fonction de la lettre placée
    if l == 1:                                                                # Si on place un "S"
        scores, lines = updateScoreS(board, n, i, j, scores, player, newLines)
    else:                                                                       # Si on place un "O"
        scores, lines = updateScoreO(board, n, i, j, scores, player, lines)
    # On s'occupe de rajouter les éléments à la liste lines
    for x in range(0, len(newLines), 1):
        lines.append(newLines[x])


    return board, lines


# Une fonction « winner(scores) » qui retourne une chaîne de caractère indiquant le résultat de la partie.
def winner(scores):
    # Retourne le numéro du joueur gagnant ou 0 si égalité
    if scores[0] > scores[1]:
        return 1
    elif scores[0] < scores[1]:
        return 2
    else:
        return 0


# Une fonction qui retourne si il est encore possible de remplir une case
def isWinner(board, n, scores):
    for x in range (0, n, 1):
        for y in range (0, n, 1):
            if board[x][y] == '0':
                return False
    return True


# Une fonction qui change le joueur actuel et retourne le nouveau joueur
def changePlayer(currentPlayer):
    if currentPlayer == 1:
        return 2
    else:
        return 1
