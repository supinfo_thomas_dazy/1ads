# SOS Game, code par Thomas Dazy et Damien SCHWARZROCK (c)
# 2018 (c)
# V2.0
# Version avec IA Simple (aléatoire)


# On importe le fichier sosAlgorithms.py:
import sosAlgorithms

import random


# • Une procédure « drawBoard(mySurface,n) » qui dessine le plateau initial.
# • Une procédure « displayScore(mySurface,n,scores) » qui affiche le score des deux joueurs.
# • Une procédure « displayPlayer(mySurface,n,player) » qui indique au joueur player que c’est
# son tour.
# • Une procédure « drawCell(mySurface,board,i,j,player) » qui dessine le contenu de la case
# de coordonnées i et j de la couleur du joueur player.
# • Une procédure « drawLines(mySurface,lines,player) » qui trace les lignes correspondant aux
# éventuels alignements dont les informations sont contenues dans la liste lines.
# • Une procédure « displayWinner(mySurface,n,scores) » qui affiche le résultat de la partie.

# On importe pygame
import pygame, sys
from pygame.locals import *
pygame.init()

# On définis les différentes couleurs pour l'affichage

WHITE   = (255,255,255)
BLACK   = (0  ,0  ,0  )
RED     = (255,0  ,0  )
BLUE    = (0  ,0  ,255)
GREEN   = (0  ,255,0  )
GREY    = (210,210,210)

### Import des polices de caractères ###
fontObj = pygame.font.Font('freesansbold.ttf',30)

# fontAwesome30 = pygame.font.Font("font_awesome.ttf", 30)

def mainMenu(mySurface):

    # Le texte SOS prend un bloc de 160 de large:
    texteSurface = fontObj.render("S",True,RED,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (410,285)
    mySurface.blit(texteSurface,texteRect)

    texteSurface = fontObj.render("O",True,BLUE,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (430,285)
    mySurface.blit(texteSurface,texteRect)

    texteSurface = fontObj.render("S",True,RED,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (455,285)
    mySurface.blit(texteSurface,texteRect)

    texteSurface = fontObj.render("GAME",True,WHITE,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (480,285)
    mySurface.blit(texteSurface,texteRect)

    pygame.draw.rect(mySurface,GREEN,(647,408,163,50))
    pygame.draw.rect(mySurface,RED,(185,408,163,50))

    # Texte START de longueur 95
    texteSurface = fontObj.render("START",True,WHITE,GREEN)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (683,420)
    mySurface.blit(texteSurface,texteRect)

    # Texte QUIT de longueur 73
    texteSurface = fontObj.render("QUIT",True,WHITE,RED)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (228,420)
    mySurface.blit(texteSurface,texteRect)


# Une procédure « drawBoard(mySurface,n) » qui dessine le plateau initial.
def drawBoard(mySurface, n):
    pygame.draw.line(mySurface, WHITE, (100, 100), (650, 100), 5)
    pygame.draw.line(mySurface, WHITE, (100, 100), (100, 550), 5)
    pygame.draw.line(mySurface, WHITE, (100, 550), (650, 550), 5)
    pygame.draw.line(mySurface, WHITE, (650, 100), (650, 550), 5)
    #Hauteur ligne
    lineHeight = (450 / n) + 1
    #longeur ligne
    lineWidth = (550 / n) + 1
    #dessin des lignes vertical
    for x in range(0, n, 1):
        posX = round((x * lineHeight) + 100)
        pygame.draw.line(mySurface, WHITE, (100, posX), (650, posX), 3)
     #dessin des lignes horizontal
    for y in range(0, n, 1):
        posY = round((y * lineWidth) + 100)
        pygame.draw.line(mySurface, WHITE, (posY, 100), (posY, 550), 3)


    fontObjS_O = pygame.font.Font('freesansbold.ttf', round((200 / n) - 5))
    #affichage des S et des O dans les cases du tableau
    for x in range(0, n, 1):
        for y in range(0, n, 1):
            positionX = ((lineWidth * y) + 105) + (lineWidth / 16)  # 105 pour l'épaisseur de la ligne
            positionY = ((lineHeight * x) + 105) + (lineHeight / 4)  # 105 pour l'épaisseur de la ligne
            texteSurface = fontObjS_O.render("S | O", True, WHITE, BLACK)
            texteRect = texteSurface.get_rect()
            texteRect.topleft = (positionX, positionY)
            mySurface.blit(texteSurface, texteRect)





# Une procédure « drawLines(mySurface,lines,player) » qui trace les lignes correspondant aux éventuels alignements dont les informations sont contenues dans la liste lines.
def drawLines(mySurface, lines, player, n):
    for x in range(0, len(lines), 1):
        hauteur_case = (450 / n) + 1
        largeur_case = (550 / n) + 1
        absice_centre1 = lines[x][1] * largeur_case + largeur_case / 2 + 100
        ordone_centre1 = lines[x][0] * hauteur_case + hauteur_case / 2 + 100
        absice_centre2 = lines[x][3] * largeur_case + largeur_case / 2 +100
        ordone_centre2 = lines[x][2] * hauteur_case + hauteur_case / 2 +100

        if player == 1 :
            color = RED
        else :
            color = BLUE
        pygame.draw.line(mySurface, color, (absice_centre1, ordone_centre1), (absice_centre2, ordone_centre2), 5)

# Fonction qui affiche les joueurs et les scores
def affichagePlayer (mySurface, scores, n, player):
    pygame.draw.rect(mySurface, BLACK, (900, 200, 100, 200))
    #affichage du scores des joueurs
    fontObj2 = pygame.font.Font('freesansbold.ttf', 36)
    if player == 1:
        backgroundColor1 = GREY
        backgroundColor2 = BLACK
    elif player == 2:
        backgroundColor1 = BLACK
        backgroundColor2 = GREY
    else:
        backgroundColor1 = BLACK
        backgroundColor2 = BLACK
    texteSurface2 = fontObj2.render("PLAYER 1 : "+str(scores[0]), True, RED, backgroundColor1)
    texteSurface3 = fontObj2.render("ORDINATEUR : "+str(scores[1]),True, BLUE, backgroundColor2)
    mySurface.blit(texteSurface2, (675, 225))
    mySurface.blit(texteSurface3, (675, 325))

# Séléction de la taille du plateau par le joueur
def selectionTaille(mySurface, n):
    #affichage des bouttons pour choisir la taille du tableau

    fontObj = pygame.font.Font('freesansbold.ttf',36)
    fontObj1 = pygame.font.Font('freesansbold.ttf',24)
    texteSurface1 =fontObj1.render("dimension :",True,WHITE)
    texteSurface = fontObj.render("-  "+str(n)+" +",True,WHITE)
    texteSurface3= fontObj.render("Commencer la partie ",True,WHITE)
    mySurface.blit(texteSurface3,(325,50))
    mySurface.blit(texteSurface,(50,50))
    mySurface.blit(texteSurface1,(35,20))

def updateCell(mySurface, n, x, y, player, value):

    fontObjS_O = pygame.font.Font('freesansbold.ttf', round((200 / n) - 5))
    lineHeight = (450 / n) + 1
    lineWidth = (550 / n) + 1

    positionX = ((lineWidth * x) + 105) + (lineWidth / 16)  # 105 pour l'épaisseur de la ligne
    positionY = ((lineHeight * y) + 105) + (lineHeight / 4)  # 105 pour l'épaisseur de la ligne
    texteSurface = fontObjS_O.render("S | O", True, BLACK, BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.topleft = (positionX, positionY)
    mySurface.blit(texteSurface, texteRect)

    # pygame.draw.rect(mySurface, WHITE, (cellPosX - ((lineWidth / 2) + 1), cellPosY - lineHeight / 2, lineWidth + 2, lineHeight - 1))

    lineWidth = round((550 / n) + 1)
    cellPosX = (100 + x * lineWidth) + (lineWidth / 2)
    lineHeight = round((450 / n) + 1)
    cellPosY = (100 + y * lineHeight) + (lineHeight / 2)
    # pygame.draw.rect(mySurface, GREEN, ((cellPosX - lineWidth / 2) + 8, (cellPosY - lineHeight / 2) + 8, lineWidth - (lineWidth * 0.19), lineHeight))


    if player == 1:
        color = RED
    else:
        color = BLUE

    if value == 1:
        letter = "S"
    else:
        letter = "O"
    fontObjS_O = pygame.font.Font('freesansbold.ttf', round((350 / n)))
    texteSurface = fontObjS_O.render(letter, True, color, BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.center = (cellPosX, cellPosY)
    mySurface.blit(texteSurface, texteRect)

# Une procédure « displayWinner(mySurface,n,scores) » qui affiche le résultat de la partie.
def displayWinner(mySurface, n, scores):
    fontObj2 = pygame.font.Font('freesansbold.ttf', 36)
    pygame.draw.rect(mySurface, BLACK, (675, 200, 325, 200))
    if scores[0] == scores[1]:
        texteSurface = fontObj2.render("Egalité ", True, WHITE)
        texteRect = texteSurface.get_rect()
        texteRect.center = (850, 300)
        mySurface.blit(texteSurface, texteRect)
    else:
        if scores[0] > scores[1]:
            texteSurface = fontObj2.render("Le joueur 1", True, RED)
            color = RED
        else:
            texteSurface = fontObj2.render("L'ordinateur", True, BLUE)
            color = BLUE
        texteRect = texteSurface.get_rect()
        texteRect.center = (800, 250)
        mySurface.blit(texteSurface, texteRect)
        texteSurface = fontObj2.render("a gagné!", True, color)
        texteRect = texteSurface.get_rect()
        texteRect.center = (900, 450)
        mySurface.blit(texteSurface, texteRect)

    texteSurface = fontObj2.render("Cliquez pour",True,WHITE,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.midbottom = (800,560)
    mySurface.blit(texteSurface,texteRect)

    texteSurface = fontObj2.render("quitter",True,WHITE,BLACK)
    texteRect = texteSurface.get_rect()
    texteRect.midtop = (900,560)
    mySurface.blit(texteSurface,texteRect)


# Une procédure « gamePlay(mySurface,board,n,scores) » qui gère une partie complète.
def gamePlay(mySurface, board, n, scores, gameState, poseX, poseY, player, lines):
    oldScores = [0, 0]
    oldScores[0] = scores[0]
    oldScores[1] = scores[1]

    if 100 < poseX < 650 and 100 < poseY < 550:
        for x in range(0, n, 1):
            for y in range(0, n, 1):
                lineWidth = round((550 / n) + 1)
                cellPosMinX = 100 + x * lineWidth
                cellPosMaxX = cellPosMinX + lineWidth
                lineHeight = round((450 / n) + 1)
                cellPosMinY = 100 + y * lineHeight
                cellPosMaxY = cellPosMinY + lineHeight
                if cellPosMinX < poseX < cellPosMaxX and cellPosMinY < poseY < cellPosMaxY:
                    # Numéro de collone: x
                    # Numéro de ligne: y
                    if cellPosMinX < poseX < (cellPosMaxX - (lineWidth / 2)):
                        # Quand on clique sur S
                        value = 1
                    else:
                        # Quand on clique sur O
                        value = 2
                    # oldScores = scores
                    # On met à jour la partie
                    if sosAlgorithms.possibleSquare(board, n, y, x):
                        board, lines = sosAlgorithms.update(board, n, y, x, value, scores, player, lines)
                        updateCell(mySurface, n, x, y, player, value)
                        drawLines(mySurface, lines, player, n)

                        # Vérifie si il y a un gagnant
                        if sosAlgorithms.isWinner(board, n, scores):
                            winnerPlayer = sosAlgorithms.winner(scores)
                            displayWinner(mySurface, n, scores)
                            gameState = 4
                        if oldScores == scores:
                            # Changement du joueur actuel:
                            player = sosAlgorithms.changePlayer(player)
    return board, player, gameState



# Une procédure « SOS(n) » qui créera une fenêtre graphique, initialisera les structures de données, et gèrera une partie complète.
def SOS(n):
    mySurface = pygame.display.set_mode((1000,600))
    pygame.display.set_caption('SOS Game Project - Supinfo')
    pygame.draw.rect(mySurface, BLACK, (0, 0, 1000, 600))

    ## On stoque le status de la partie
    gameState = 0
    # 0 = Menu principal
    # 1 = Menu secondaire (on progress)
    # 2 = Sélection de la taille du tableau
    # 3 = Partie en cours
    # 4 = Partie terminée

    # On initialise les variables nécessaires
    scores = [0, 0]
    player = 1


    # Affichage du menu principal:
    if gameState == 0:
        mainMenu(mySurface)



    # Tant que l'on ne quite pas la fenêtre, on continue de l'afficher
    inProgress = True
    while inProgress:
        lines = []

        # Pas encore de menu intermédiaire
        if gameState == 1:
            gameState = 2

        if gameState == 2:
            pygame.draw.rect(mySurface, BLACK, (0, 0, 1000, 600))
            drawBoard(mySurface, n)
            selectionTaille(mySurface, n)

        if gameState == 3:
            # drawBoard(mySurface, n)
            affichagePlayer(mySurface, scores, n, player)

        # if gameState == 4:
        #     print("FIN DU PROGRAMME")

        # Si on quite la fenêtre alors on quite le jeu:
        for event in pygame.event.get():
            if event.type == QUIT:
                inProgress = False

            if event.type == MOUSEBUTTONUP:
                # Si on clique, les positions du clic sont poseX et poseY
                poseX, poseY = event.pos

                # Si on est dans le menu principal:
                if gameState == 0:
                    # Si on clique sur "QUIT"
                    if 185 <= poseX <= 348 and 408 <= poseY <= 458:
                        inProgress = False

                    # Si on clique sur "START"
                    if 647 <= poseX <= 810 and 408 <= poseY <= 458:
                        gameState = 1
                        pygame.draw.rect(mySurface,BLACK,(0,0,1000,600))
                        drawBoard(mySurface, n)

                if gameState == 2:
                    # Si on clique sur '-'
                    if poseX > 42 and poseY > 54 and poseX < 77 and poseY < 80 and n > 4:
                        n = n - 1
                    # Si on clique sur '+'
                    if poseX > 118 and poseY > 54 and poseX < 152 and poseY < 80 and n < 7:
                        n = n + 1
                    # Si on clique sur 'Commencer la partie'
                    if poseX > 317 and poseY > 54 and poseX < 709 and poseY < 90:
                        # On génère le plateau de jeu
                        board = sosAlgorithms.newBoard(n)
                        # On l'affiche
                        pygame.draw.rect(mySurface, BLACK, (0, 0, 1000, 600))
                        drawBoard(mySurface, n)
                        gameState = 3

                if gameState == 3 and player == 1:
                    board, player, gameState = gamePlay(mySurface, board, n, scores, gameState, poseX, poseY, player, lines)

            if event.type == MOUSEBUTTONDOWN:
                if gameState == 4:
                    if poseX > 0 and poseY > 0:
                        inProgress = False

        if gameState == 3 and player == 2:
            oldScores = [0, 0]
            oldScores[0] = scores[0]
            oldScores[1] = scores[1]

            autoSelectedCaseX = random.randint(0,n-1)
            autoSelectedCaseY = random.randint(0,n-1)
            value = random.randint(1,2)
            while sosAlgorithms.possibleSquare(board, n, autoSelectedCaseY, autoSelectedCaseX) == False:
                autoSelectedCaseX = random.randint(0,n-1)
                autoSelectedCaseY = random.randint(0,n-1)

            if sosAlgorithms.possibleSquare(board, n, autoSelectedCaseY, autoSelectedCaseX):
                board, lines = sosAlgorithms.update(board, n, autoSelectedCaseY, autoSelectedCaseX, value, scores, player, lines)
                updateCell(mySurface, n, autoSelectedCaseX, autoSelectedCaseY, player, value)
                drawLines(mySurface, lines, player, n)

                # Vérifie si il y a un gagnant
                if sosAlgorithms.isWinner(board, n, scores):
                    winnerPlayer = sosAlgorithms.winner(scores)
                    displayWinner(mySurface, n, scores)
                    gameState = 4
            if oldScores == scores:
                # Changement du joueur actuel:
                player = sosAlgorithms.changePlayer(player)



        pygame.display.update()

    if inProgress == False:
        pygame.quit()


# Taille initialle totalement arbitraire
SOS(5)




### INDEX OF USEFUL COMMAND
## Pour remplir la fenêtre en noir:
# mySurface.fill(BLACK)


# Affichage du bord pour les tests
# for x in range(0, len(board), 1):
#     print(board[x])
# print(lines)






###### Pour importer un autre fichier en python: ######
# Créer le fichier au même emplacement que le programme et lui donner l'extension ".py" (exemple: fichierAImporter.py)
# Dans le programme principal: import NOM_DU_FICHIER_SANS_EXTENSION (exemple: import fichierAImporter)
# Quand on appelle une fonction dans ce fichier, on ecris le nom du fichier sans l'extension suivi de "." suivi du nom de la fonction (exemple: fichierAImporter.maFonction())

# Exemple:

### Fichier à importer (fichierAImporter.py):
# def maFonction(duTexte):
#     return duTexte

### Fichier principal:
# import fichierAImporter
# print(fichierAImporter.maFonction("Coucou"))

# Résultat sur la console:
# >>> Coucou
